export PW=`pwgen -Bs 10 1`
echo $PW > password

rm -Rf main/resources/keyStore/*
rm -Rf main/resources/trustStore/*
rm -Rf main/resources/certificates/*

# Create a certificate and put it in the server key store
keytool -genkeypair -v \
  -alias moita-ca \
  -dname "CN=Raphael Moita, OU=Lab, O=Moita Lab, C=PL" \
  -keystore main/resources/keyStore/moita.jks \
  -keypass:env PW \
  -storepass:env PW \
  -keyalg RSA \
  -keysize 4096 \
  -validity 9999

keytool -genkeypair -v \
  -alias localhost \
  -dname "CN=localhost, OU=Lab, O=Moita Lab, C=PL" \
  -keystore main/resources/keyStore/moita.jks \
  -keypass:env PW \
  -storepass:env PW \
  -keyalg RSA \
  -keysize 4096 \
  -validity 9999

# Export the 'moita-ca' public certificate as moita.crt so that it can be used in trust stores.
keytool -export -v \
  -alias moita-ca \
  -file main/resources/certificates/moita.crt \
  -keypass:env PW \
  -storepass:env PW \
  -keystore main/resources/keyStore/moita.jks \
  -rfc

keytool -export -v \
  -alias localhost \
  -file main/resources/certificates/localhost.crt \
  -keypass:env PW \
  -storepass:env PW \
  -keystore main/resources/keyStore/moita.jks \
  -rfc

# Tell main/resources/trustStore/moita.jks it can trust moita-ca as a signer.
keytool -import -v \
  -alias moita-ca \
  -file main/resources/certificates/moita.crt \
  -keystore main/resources/trustStore/moita.jks \
  -storetype JKS \
  -storepass:env PW

keytool -import -v \
  -alias localhost \
  -file main/resources/certificates/localhost.crt \
  -keystore main/resources/trustStore/moita.jks \
  -storetype JKS \
  -storepass:env PW

keytool -list -v \
  -keystore main/resources/trustStore/moita.jks \
  -storepass:env PW
