package org.moita.webhttps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

@SpringBootApplication
public class WebHttpsApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebHttpsApplication.class, args);
	}
}
