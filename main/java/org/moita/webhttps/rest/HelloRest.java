package org.moita.webhttps.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/hello")
public class HelloRest
{
    @GetMapping("/")
    public String hello(@RequestHeader Map<String, String> headers) {

        headers.forEach((key, value) -> {
            System.out.println("Header "+ key+" = "+ value);
        });

        return "Hello " + System.currentTimeMillis();
    }
}
