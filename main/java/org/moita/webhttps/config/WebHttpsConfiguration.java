package org.moita.webhttps.config;

//import io.netty.handler.ssl.SslContext;
//import io.netty.handler.ssl.SslContextBuilder;
//import io.netty.handler.timeout.ReadTimeoutHandler;
//import io.netty.handler.timeout.WriteTimeoutHandler;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.web.embedded.netty.NettyReactiveWebServerFactory;
//import org.springframework.boot.web.reactive.function.client.WebClientCustomizer;
//import org.springframework.boot.web.server.ErrorPage;
//import org.springframework.boot.web.server.Ssl;
//import org.springframework.boot.web.server.WebServerFactoryCustomizer;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.client.reactive.ClientHttpConnector;
//import org.springframework.http.client.reactive.ReactorClientHttpConnector;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.util.ResourceUtils;
//import org.springframework.web.reactive.function.client.WebClient;
//import reactor.netty.http.client.HttpClient;
//
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.security.*;
//import java.security.cert.Certificate;
//import java.security.cert.CertificateException;
//import java.security.cert.X509Certificate;
//import java.util.Arrays;
//import java.util.Collections;
//import java.util.stream.Collectors;
//
//import static io.netty.channel.ChannelOption.CONNECT_TIMEOUT_MILLIS;
//import static java.time.Duration.ofMillis;
//import static java.util.concurrent.TimeUnit.MILLISECONDS;

//@Configuration
public class WebHttpsConfiguration
{

//
//    @Bean
//    public WebClient webClient(@Value("${server.ssl.trust-store}") String trustStorePath,
//                               @Value("${server.ssl.trust-store-password}") String trustStorePass,
//                               @Value("${server.ssl.key-store}") String keyStorePath,
//                               @Value("${server.ssl.key-store-password}") String keyStorePass,
//                               @Value("${server.ssl.key-alias}") String keyAlias)
//    {
//        ClientHttpConnector clientHttpConnector =
//                clientHttpConnector(trustStorePath, trustStorePass, keyStorePath, keyStorePass, keyAlias);
//
//        HttpClient httpClient = HttpClient.create()
////                .secure(t ->
////                {
////                    try
////                    {
////                        t.sslContext(SslContextBuilder.forClient()
////                                .trustManager(InsecureTrustManagerFactory.INSTANCE)
////                                .build());
////                    } catch (SSLException e)
////                    {
////                        e.printStackTrace();
////                    }
////                })
//                .option(CONNECT_TIMEOUT_MILLIS, 5000)
//                .responseTimeout(ofMillis(5000))
//                .doOnConnected(conn ->
//                        conn.addHandlerLast(new ReadTimeoutHandler(5000, MILLISECONDS))
//                                .addHandlerLast(new WriteTimeoutHandler(5000, MILLISECONDS)));
//
//        return WebClient.builder()
////                .clientConnector(new ReactorClientHttpConnector(httpClient))
//                .clientConnector(clientHttpConnector)
//                .build();
//    }

//    ClientHttpConnector clientHttpConnector(@Value("${server.ssl.trust-store}") String trustStorePath,
//                                            @Value("${server.ssl.trust-store-password}") String trustStorePass,
//                                            @Value("${server.ssl.key-store}") String keyStorePath,
//                                            @Value("${server.ssl.key-store-password}") String keyStorePass,
//                                            @Value("${server.ssl.key-alias}") String keyAlias)
//    {
//        try
//        {
//            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
//            trustStore.load(new FileInputStream(ResourceUtils.getFile(trustStorePath)), trustStorePass.toCharArray());
//
//            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
//            keyStore.load(new FileInputStream(ResourceUtils.getFile(keyStorePath)), keyStorePass.toCharArray());
//
//            X509Certificate[] certificates = Collections.list(trustStore.aliases())
//                    .stream()
//                    .filter(t ->
//                    {
//                        try
//                        {
//                            return trustStore.isCertificateEntry(t);
//                        } catch (KeyStoreException e1)
//                        {
//                            throw new RuntimeException("Error reading truststore", e1);
//                        }
//                    })
//                    .map(t ->
//                    {
//                        try
//                        {
//                            return trustStore.getCertificate(t);
//                        } catch (KeyStoreException e2)
//                        {
//                            throw new RuntimeException("Error reading truststore", e2);
//                        }
//                    })
//                    .map(c -> (X509Certificate) c).toArray(X509Certificate[]::new);
//
//            PrivateKey privateKey = (PrivateKey) keyStore.getKey(keyAlias, keyStorePass.toCharArray());
//            Certificate[] certChain = keyStore.getCertificateChain(keyAlias);
//
//            X509Certificate[] x509CertificateChain = Arrays.stream(certChain)
//                    .map(certificate -> (X509Certificate) certificate)
//                    .collect(Collectors.toList())
//                    .toArray(new X509Certificate[certChain.length]);
//
//            SslContext sslContext = SslContextBuilder.forClient()
//                    .keyManager(privateKey, keyStorePass, x509CertificateChain)
//                    .trustManager(certificates)
//                    .build();
//
//            HttpClient httpClient = HttpClient.create()
//                    .secure(sslContextSpec -> sslContextSpec.sslContext(sslContext));
//
//            return new ReactorClientHttpConnector(httpClient);
//
//        }
//        catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException | UnrecoverableKeyException e)
//        {
//            throw new RuntimeException(e);
//        }
//    }
//
//    //@Bean
//    WebClientCustomizer configureWebclient(@Value("${server.ssl.trust-store}") String trustStorePath,
//                                           @Value("${server.ssl.trust-store-password}") String trustStorePass,
//                                           @Value("${server.ssl.key-store}") String keyStorePath,
//                                           @Value("${server.ssl.key-store-password}") String keyStorePass,
//                                           @Value("${server.ssl.key-alias}") String keyAlias)
//    {
//
//        return (WebClient.Builder webClientBuilder) ->
//        {
//            SslContext sslContext;
//            final PrivateKey privateKey;
//            final X509Certificate[] certificates;
//            try
//            {
//                final KeyStore trustStore;
//                final KeyStore keyStore;
//                trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
//                trustStore.load(new FileInputStream(ResourceUtils.getFile(trustStorePath)), trustStorePass.toCharArray());
//                keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
//                keyStore.load(new FileInputStream(ResourceUtils.getFile(keyStorePath)), keyStorePass.toCharArray());
//
//                certificates = Collections.list(trustStore.aliases())
//                        .stream()
//                        .filter(t ->
//                        {
//                            try
//                            {
//                                return trustStore.isCertificateEntry(t);
//                            } catch (KeyStoreException e1)
//                            {
//                                throw new RuntimeException("Error reading truststore", e1);
//                            }
//                        })
//                        .map(t ->
//                        {
//                            try
//                            {
//                                return trustStore.getCertificate(t);
//                            } catch (KeyStoreException e2)
//                            {
//                                throw new RuntimeException("Error reading truststore", e2);
//                            }
//                        })
//                        .map(c -> (X509Certificate) c).toArray(X509Certificate[]::new);
//
//                privateKey = (PrivateKey) keyStore.getKey(keyAlias, keyStorePass.toCharArray());
//                Certificate[] certChain = keyStore.getCertificateChain(keyAlias);
//
//                X509Certificate[] x509CertificateChain = Arrays.stream(certChain)
//                        .map(certificate -> (X509Certificate) certificate)
//                        .collect(Collectors.toList())
//                        .toArray(new X509Certificate[certChain.length]);
//
//                sslContext = SslContextBuilder.forClient()
//                        .keyManager(privateKey, keyStorePass, x509CertificateChain)
//                        .trustManager(certificates)
//                        .build();
//
//                HttpClient httpClient = HttpClient.create()
//                        .secure(sslContextSpec -> sslContextSpec.sslContext(sslContext));
//
//                ClientHttpConnector connector = new ReactorClientHttpConnector(httpClient);
//                webClientBuilder.clientConnector(connector);
//            } catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException | UnrecoverableKeyException e)
//            {
//                throw new RuntimeException(e);
//            }
//        };
//    }
//
//    //@Bean
//    public WebServerFactoryCustomizer<NettyReactiveWebServerFactory> customizer()
//    {
//        return new WebServerFactoryCustomizer<NettyReactiveWebServerFactory>()
//        {
//            @Override
//            public void customize(NettyReactiveWebServerFactory factory)
//            {
//                Ssl ssl = new Ssl();
//                // Your SSL Cusomizations
//                ssl.setEnabled(true);
//                ssl.setKeyStore("/path/to/keystore/keystore.jks");
//                ssl.setKeyAlias("alias");
//                ssl.setKeyPassword("password");
//                factory.setSsl(ssl);
//                factory.addErrorPages(new ErrorPage("/errorPage"));
//            }
//        };
//    }
}
